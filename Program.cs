﻿using System;
using System.IO;
using TestOPC.Clients;

namespace TestOPC {
    internal class Program {
        static void Main(string[] args) {
            string endpoint = "opcda://localhost/opcserversim.Instance.1";
            if(args.Length > 0) 
                endpoint = args[0];
            IClient client = new ClientOpcDA(endpoint, OutputMethod);
            client.Init();
            client.Connect();
            client.GetTagsList();
            client.GetTagValue();
            client.SubscribeTag();
            Console.WriteLine("Для завершения нажмите ENTER");
            Console.ReadLine();
            client.Disconnect();
        }

        static public void OutputMethod(string fileName, string data) {
            fileName = $"./{fileName}.txt";
            try {
                File.WriteAllText(fileName, data);
            } catch(Exception ex) {
                Console.WriteLine($"Save to file: {fileName}, failed!");
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
