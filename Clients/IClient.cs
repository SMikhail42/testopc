﻿namespace TestOPC.Clients {
    internal interface IClient {
        void Init();

        void Connect();

        void Disconnect();

        void GetTagsList();

        void GetTagValue();

        void SubscribeTag();
    }
}
