﻿using Opc.Da;
using OpcCom;
using System;
using System.Net;

namespace TestOPC.Clients {
    internal class ClientOpcDA : IClient {
        private Opc.Da.Server Server { get; set; }

        private Opc.ConnectData ConnectData { get; set; }

        private BrowseElement[] Elements { get; set; } = new BrowseElement[0];

        private Opc.Da.Subscription Sub { get; set; }

        public Opc.URL Url { get; set; }

        public NetworkCredential AuthData { get; set; } = new NetworkCredential();

        public OpcCom.Factory ServerFactory { get; set; } = new OpcCom.Factory();


        public delegate void OutData(string type, string data);
        
        public event OutData EventGetData;

        public ClientOpcDA(string url, OutData handler) {
            Url = new Opc.URL(url);
            EventGetData += handler;
        }

        private void DoSafe(string msgErr, Action doSomething) {
            try {
                doSomething();
            } catch(Exception ex) {
                Console.WriteLine(msgErr);
                Console.WriteLine(ex.ToString());
            }
        }

        private string ListTagsToString() {
            string res = "";
            foreach(var element in Elements)
                res += $"{element.Name}\n\r";
            return res;
        }

        private Opc.Da.Item GetTagItem(int num) {
            return new Item() {
                ItemName = Elements[num].ItemName,
                Active = true,
            };
        }

        public void Init() {
            Server = new Opc.Da.Server(ServerFactory, null);
            ConnectData = new Opc.ConnectData(AuthData);
        }

        public void Connect() {
            DoSafe("Connect to server failed!", () => {
                Server.Connect(Url, ConnectData);
            });
        }

        public void Disconnect() {
            DoSafe("Disconnect server failed!", () => {
                if(Server.IsConnected) Server.Disconnect();
            });
        }    

        public void GetTagsList() {
            DoSafe("Get Tags List failed!", () => {
                BrowseFilters filters = new BrowseFilters() { BrowseFilter = browseFilter.item };
                Elements = Server.Browse(null, filters, out BrowsePosition position);
            });
            EventGetData.Invoke("TagsList", ListTagsToString());
        }

        public void GetTagValue() {
            string res = "";
            if(Elements.Length > 0) {
                var items = new Item[1] { GetTagItem(0) };
                DoSafe("Get Tags List failed!", () => {
                    res = Server.Read(items)[0].Value.ToString();
                });
            }
            EventGetData.Invoke("TagValue", res);
        }

        public void SubscribeTag() {
            Sub = (Opc.Da.Subscription)Server.CreateSubscription(new SubscriptionState());
            if(Elements.Length > 0)  Sub.AddItems(new Item[] { GetTagItem(0) });
            Sub.DataChanged += DataTagChanged;
        }

        public void DataTagChanged(object subscriptionHandle,
            object requestHandle, Opc.Da.ItemValueResult[] values) {
            EventGetData.Invoke("SubscribeTagValue",
                (values.Length > 0) ? values[0].Value.ToString() : "");
        }
    }
}
